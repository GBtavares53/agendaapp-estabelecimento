const routes = [
  {
    path: '/',
    redirect: '/LoginInicio'
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'ProdutosProcedimentos', name: 'ProdutosProcedimentos', component: () => import('pages/ProdutosProcedimentos.vue'), meta: { requiresAuth: true } },
      { path: 'BloquearHorario', name: 'BloquearHorario', component: () => import('src/pages/BloquearHorário.vue'), meta: { requiresAuth: true } },
      { path: 'Agendados', name: 'Agendados', component: () => import('pages/AgendadosCliente.vue'), meta: { requiresAuth: true } },
      { path: 'ProdutosCadastrados', name: 'ProdutosCadastrados', component: () => import('pages/ProdutosCadastrados.vue'), meta: { requiresAuth: true } },
      { path: 'Disponibilidade', name: 'DisponibilidadeHorario', component: () => import('pages/DisponibilidadeHorario.vue'), meta: { requiresAuth: true } },
      { path: 'Horarios', name: 'Horarios', component: () => import('pages/HorariosDisponiveis.vue'), meta: { requiresAuth: true } }
    ]
  },
  {
    path: '/LoginInicio',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '/LoginInicio', name: 'Login', component: () => import('src/pages/LoginInicio.vue') }
    ]
  },
  {
    path: '',
    redirect: '/LoginInicio',
    component: () => import('pages/ErrorNotFound.vue')
  }

]

export default routes
